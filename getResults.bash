#!/bin/bash

difficulty="hard"
data="random"

for i in ./reseni/heuristika/${data}/${difficulty}/*; do cat "$i" ;done | grep 'Starting' | tr -d 'a-zA-Z: .' | uniq > ./"start${data}${difficulty}.txt"

for prog in {"heuristika","genetika","genetikaKrocena"}
do

for i in ./reseni/${prog}/${data}/${difficulty}/*
do
	cat "$i" | grep 'Communication' | tr -d 'a-zA-Z: .' >> ./"${prog}${data}${difficulty}Res.txt"
	echo "()" >> ./"${prog}${data}${difficulty}Res.txt"

done

for i in ./reseni/${prog}/${data}/${difficulty}/*
do
	cat "$i" | grep 'RAM' | tr -d 'a-zA-Z: .' >> ./"${prog}${data}${difficulty}RAM.txt"
	echo "()" >> ./"${prog}${data}${difficulty}RAM.txt"

done




done
