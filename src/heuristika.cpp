#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <numeric>
#include <algorithm>
#include <utility>
#include <cassert>

int clusterLineParam, clusterIncreaseParam;

int numNodes = -1, nodeCores = -1, nodeRAM = -1, numVMs = -1;

std::vector <std::vector <int>> nodeDistances;

int VMRAMdistance;

int clusterCommLine = -1;

class virtualMachine {
public:
	int cores, RAM, nodePlacement;
	std::vector <int> communication;

	virtualMachine(int c, int r, int n):cores(c), RAM(r), nodePlacement(n){
		communication.reserve(numVMs);
	}

	virtualMachine(const virtualMachine & vm){
		cores = vm.cores;
		RAM = vm.RAM;
		nodePlacement = vm.nodePlacement;
		communication = vm.communication;
	}
};

class node {
public:
	int cores;
	int RAM;

	node():cores(0),RAM(0){}

	node(const node & n){
		cores = n.cores;
		RAM = n.RAM;
	}
};

std::vector <node> startingFilling;

std::vector <virtualMachine> startingPlacement;

int readDTC(char * filePath){

	std::fstream file;
	file.open(filePath, std::fstream::in);
	if(!file.is_open()){
		std::cerr << "Failed to open datacentre file" << std::endl;
		return -1;
	}

	std::string tmp;

	file >> tmp >> numNodes;
	if(!file.good() || tmp != "#nodes:" || numNodes <= 0){
		std::cerr << "Error reading numNodes" << std::endl;
		file.close();
		return -2;
	}

	file >> tmp >> nodeCores;
	if(!file.good() || tmp != "#nodeCores:" || nodeCores <= 0){
		std::cerr << "Error reading nodeCores" << std::endl;
		file.close();
		return -3;
	}

	file >> tmp >> nodeRAM;
	if(!file.good() || tmp != "#nodeRAM:" || nodeRAM <= 0){
		std::cerr << "Error reading nodeRAM" << std::endl;
		file.close();
		return -4;
	}

	file >> tmp;
	if(!file.good() || tmp != "NodeDistances"){
		std::cerr << "Error reading NodeDistances" << std::endl;
		file.close();
		return -5;
	}

	{
		std::vector <int> tmpV;
		for(int i = 0; i < numNodes; ++i){
			tmpV.push_back(0);
		}
		for(int i = 0; i < numNodes; ++i){
			nodeDistances.push_back(tmpV);
		}
	}

	char tmpC1 = 'x', tmpC2 = 'x';
	int id = -1, dist = -1;

	for(int j = 0; j < numNodes; ++j){

		file >> tmp >> tmpC1 >> id;
		if(!file.good() || tmp != "NodeName:" || tmpC1 != 'n' || id <= 0 || id > numNodes){
			std::cerr << "Error reading NodeName" << std::endl;
			file.close();
			return -6;
		}
		file >> tmp;
		if(!file.good() || tmp != "Distances:"){
			std::cerr << "Error reading Distances" << std::endl;
			file.close();
			return -7;
		}
		for(int i = 0; i < numNodes; ++i){
			file >> tmpC1 >> id >> tmpC2 >> dist;
			if(!file.good() || tmpC1 != 'n' || tmpC2 != '-' || dist < 0 || id <= 0 || id > numNodes){
				std::cerr << "Error reading distance" << std::endl;
				file.close();
				return -8;
			}
			nodeDistances[j][id-1] = dist;
		}

	}

	file >> tmp >> numVMs;
	if(!file.good() || tmp != "#virtualMachines:" || numVMs <= 0){
		std::cerr << "Error reading number of virtual machines" << std::endl;
		file.close();
		return -9;
	}
	startingPlacement.reserve(numVMs);
	startingFilling.reserve(numNodes);
	for(int i = 0; i < numNodes; ++i){
		startingFilling.emplace_back();
	}
	
	int nodeID = 0, c = -1, r = -1, peekI = -1;
	for(int i = 0; i < numVMs; ++i){
		file >> c >> tmpC1 >> r;
		if(!file.good() || tmpC1 != '-' || c <= 0 || r <= 0){
			std::cerr << "Error reading VM params" << std::endl;
			file.close();
			return -10;
		}

		startingPlacement.emplace_back(c, r, nodeID);
		startingFilling[nodeID].cores += c;
		startingFilling[nodeID].RAM += r;
		
		file.get(tmpC1);
		if(!file.good() || tmpC1 != ' '){
			std::cerr << "Error reading VMs" << std::endl;
			file.close();
			return -11;
		}
		peekI = file.peek();
		if(peekI == (int)'\n'){
			++nodeID;
		}
	}

	file >> tmp;
	if(!file.good() || tmp != "CommunicationMatrix:"){
		std::cerr << "Error reading Communication matrix" << std::endl;
		file.close();
		return -12;
	}

	int comm;
	for(int j = 0; j < numVMs; ++j){
		for(int i = 0; i < numVMs; ++i){
			file >> comm;
			if(!file.good() || comm < 0){
				std::cerr << "Error reading communication value" << std::endl;
				file.close();
				return -13;
			}

			startingPlacement[j].communication.push_back(comm);
		}
	}

	file.close();
	return 0;
}

class cluster {
public:
	int comm;
	std::vector <int> VMs;

	cluster():comm(0){};

	void countComm(){
		comm = 0;

		for(int i = 0; i < (int)VMs.size(); ++i){
			for(int j = i+1; j < (int)VMs.size(); ++j){
				comm += startingPlacement[VMs[i]].communication[VMs[j]];
			}
		}
	}
};

void selectClusterLine(){
	if(startingPlacement.size() < 2){
		std::cerr << "Fatal error..." << std::endl;
		exit(-1);
	}

	int mean = std::accumulate(startingPlacement[0].communication.begin(), startingPlacement[0].communication.end(), 0) / startingPlacement[0].communication.size();
	int max = startingPlacement[0].communication[std::max_element(startingPlacement[0].communication.begin(), startingPlacement[0].communication.end()) - startingPlacement[0].communication.begin()];
		
	for(int i = 1; i < numVMs; ++i){
		int tmp1 = std::accumulate(startingPlacement[i].communication.begin(), startingPlacement[i].communication.end(), 0) / startingPlacement[i].communication.size();
		int tmp2 = startingPlacement[i].communication[std::max_element(startingPlacement[i].communication.begin(), startingPlacement[i].communication.end()) - startingPlacement[i].communication.begin()];

		mean += tmp1;
		mean = mean / 2;
		max = (max > tmp2) ? max : tmp2; 
	}

	clusterCommLine = max - (max-mean)/clusterLineParam;
}

void getPartners(std::vector<std::pair<int, int>> & partners){
	for(int i = 0; i < numVMs; ++i){
		for(int j = 0; j < numVMs; ++j){
			if(startingPlacement[i].communication[j] > clusterCommLine){
				if(std::find(partners.begin(), partners.end(), std::make_pair(j, i)) == partners.end())
					partners.emplace_back(i, j);
			}
		}
	}
}

void getFirstClusters(std::vector<cluster> & tmp, std::vector<std::pair<int, int>> & partners){
	for(int i = partners.size()-1; i >= 0; --i){
		int f1 = -1, f2 = -1;
		for(int j = 0; j < (int)tmp.size(); ++j){
			if(std::find(tmp[j].VMs.begin(), tmp[j].VMs.end(), partners[i].first) != tmp[j].VMs.end()){	
				assert(f1 == -1);
				f1 = j;
			}

			if(std::find(tmp[j].VMs.begin(), tmp[j].VMs.end(), partners[i].second) != tmp[j].VMs.end()){
				assert(f2 == -1);
				f2 = j;
			}
		}


		if(f1 == -1 && f2 == -1){
			tmp.emplace_back();
			tmp.back().VMs.push_back(partners[i].first);
			tmp.back().VMs.push_back(partners[i].second);
		}else if(f1 != -1 && f2 == -1){
			tmp[f1].VMs.push_back(partners[i].second);
		}else if(f1 == -1 && f2 != -1){
			tmp[f2].VMs.push_back(partners[i].first);
		}else{
			tmp[f1].VMs.insert(tmp[f1].VMs.end(), tmp[f2].VMs.begin(), tmp[f2].VMs.end());
			tmp.erase(tmp.begin() + f2);
		}
		partners.pop_back();
	}

	for(auto & it : tmp){
		it.countComm();
	}
}

bool isInCluster (std::vector<cluster> & tmp, int index){
	for(auto & it : tmp){
		for(auto & iit : it.VMs){
			if(iit == index)
				return true;
		}
	}

	return false;
}

void increaseClusters(std::vector<cluster> & tmp){
	for(auto & it : tmp){
		for(int i = 0; i < (int)it.VMs.size(); ++i){
			for(int j = 0; j < numVMs; ++j){
				if(it.comm + startingPlacement[i].communication[j] > it.comm + it.comm/clusterIncreaseParam && !isInCluster(tmp,j)/*std::find(it.VMs.begin(), it.VMs.end(), j) == it.VMs.end()*/){
					it.VMs.push_back(j);
					it.countComm();
				}
			}
		}
	}
}

std::vector<cluster> createClusters(){

	selectClusterLine();

	std::vector<std::pair<int, int>> partners;
	getPartners(partners);

	std::vector<cluster> clus;
	getFirstClusters(clus, partners);

	increaseClusters(clus);

//------------------------------------------------------------------------
/*	std::cout << std::endl;
	for(auto it : clus){
		for(auto iit : it.VMs){
			std::cout << iit << " ";
		}
		std::cout << "--" << it.comm << std::endl;
	}*/
//------------------------------------------------------------------------

	return 	clus;
}

class countingNode {
public:
	int cores, RAM;
	std::vector<int> clusterMember;
	std::vector<std::pair<int, double>> attemptedStandalone; 
	std::vector<int> standalone;

	countingNode():cores(0),RAM(0){}
};

int nearestGoodNode(int idealNode, int toPlace, std::vector<countingNode> & compute){

	if(compute[idealNode].cores + startingPlacement[toPlace].cores <= nodeCores && compute[idealNode].RAM + startingPlacement[toPlace].RAM <= nodeRAM ){
		return idealNode;
	}else{
		int tmp = -1;

		for(int i = 0; i < numNodes; ++i){
			if(compute[i].cores + startingPlacement[toPlace].cores <= nodeCores && compute[i].RAM + startingPlacement[toPlace].RAM <= nodeRAM){
				if(tmp == -1){
					tmp = i;
				}else{
					if(nodeDistances[idealNode][i] < nodeDistances[idealNode][tmp])
						tmp = i;
				}
			}
		}		

		if(tmp == -1){
			std::cerr << "Fatal error" << std::endl;
			exit(666);
		}
		return tmp;
	}
}

int nodeWSpace(int toPlace, std::vector<countingNode> & compute){

	if(compute[startingPlacement[toPlace].nodePlacement].cores + startingPlacement[toPlace].cores <= nodeCores && compute[startingPlacement[toPlace].nodePlacement].RAM + startingPlacement[toPlace].RAM <= nodeRAM ){
		return toPlace;
	}else{
		int tmp = -1;

		for(int i = 0; i < numNodes; ++i){
			if(compute[i].cores + startingPlacement[toPlace].cores <= nodeCores && compute[i].RAM + startingPlacement[toPlace].RAM <= nodeRAM){
				if(tmp == -1){
					tmp = i;
				}else{
					if(compute[i].RAM + startingPlacement[toPlace].RAM > compute[tmp].RAM + startingPlacement[toPlace].RAM){
						tmp = i;
					}/*else if(compute[i].cores + startingPlacement[toPlace].cores > compute[tmp].cores + startingPlacement[toPlace].cores){
						tmp = i;
					}*/
				}
			}
		}		

		if(tmp == -1){
			std::cerr << "Fatal error" << std::endl;
			exit(666);
		}
		return tmp;
	}
}

void findPlaceCluster(cluster & cls, std::vector<countingNode> & compute, std::vector <int> & solution){
	int maxOnNode = startingPlacement[cls.VMs[0]].nodePlacement;
	int maxRAM = nodeRAM - startingFilling[maxOnNode].RAM;
	for(auto & it : cls.VMs){
		if(nodeRAM - startingFilling[startingPlacement[it].nodePlacement].RAM > maxRAM){
			maxRAM = nodeRAM - startingFilling[startingPlacement[it].nodePlacement].RAM;
			maxOnNode = startingPlacement[it].nodePlacement;
		}
	}

	for(auto & it : cls.VMs){
		int n = nearestGoodNode(maxOnNode, it, compute);	
		compute[n].clusterMember.push_back(it);
		compute[n].cores += startingPlacement[it].cores;
		compute[n].RAM += startingPlacement[it].RAM;
		solution[it] = -1;
	}
}

double countCommRatio(int vm){
	double inNode = 0;
	double outsideNode = 0;

	for(int i = 0; i < numVMs; ++i){
		if(startingPlacement[i].nodePlacement == startingPlacement[vm].nodePlacement){
			inNode += (double)startingPlacement[vm].communication[i];
		}else{
			outsideNode += (double)startingPlacement[vm].communication[i];
		}
	}

	return inNode/outsideNode;
}

bool sortBySecond(const std::pair<int, double> & a, const std::pair<int, double> & b){
	return a.second > b.second;
}

void placeStandalones(std::vector<countingNode> & compute, std::vector <int> & solution){

	for(auto & it : solution){
		if(it == -1)
			continue;

		compute[startingPlacement[it].nodePlacement].attemptedStandalone.emplace_back(it, countCommRatio(it));
		solution[it] = -1;
	}

	for(auto & it : compute){
		std::sort(it.attemptedStandalone.begin(), it.attemptedStandalone.end(), sortBySecond);
		while(!it.attemptedStandalone.empty()){

			int tmp = it.attemptedStandalone.back().first;

			if((it.cores + startingPlacement[tmp].cores) > nodeCores || (it.RAM + startingPlacement[tmp].RAM) > nodeRAM){
				break;
			}

			it.cores += startingPlacement[tmp].cores;
			it.RAM += startingPlacement[tmp].RAM;
			it.standalone.push_back(tmp);
			it.attemptedStandalone.pop_back();
		}
	}


	for(auto & it : compute){
		while(!it.attemptedStandalone.empty()){
			int tmp = it.attemptedStandalone.back().first;
			it.attemptedStandalone.pop_back();

			int n = nodeWSpace(tmp, compute);
			compute[n].standalone.push_back(tmp);
			compute[n].cores += startingPlacement[tmp].cores;
			compute[n].RAM += startingPlacement[tmp].RAM;
		}
	}
}

void placeClusters(std::vector<cluster> & clus, std::vector <int> & solution){

	std::vector<countingNode> compute;
	compute.reserve(numNodes);
	for(int i = 0; i < numNodes; ++i)
		compute.emplace_back();

	for(auto & it : clus){
		findPlaceCluster(it, compute, solution);
	}

	/*for(auto & it : solution){
		if(it == -1)
			continue;

		int n = nodeWSpace(it, compute);
		compute[n].standalone.push_back(it);
		compute[n].cores += startingPlacement[it].cores;
		compute[n].RAM += startingPlacement[it].RAM;
		solution[it] = -1;
	}*/

	placeStandalones(compute, solution);

	for(int i = 0; i < numNodes; ++i){
		for(auto & iit : compute[i].clusterMember)
			solution[iit] = i;
		for(auto & iit : compute[i].standalone)
			solution[iit] = i;
	}
}

void printUsage(char * name){
	std::cout << "Usage: " << name << " <.DTC file path> clusterLineParam clusterIncreaseParam" << std::endl;
}

long long int getCommSum(std::vector<int> & chromozome){
	long long int tmp = 0;

	for(int i = 0; i < numVMs; ++i){
		for(int j = 0; j < numVMs; ++j){
			tmp += (long long int)(nodeDistances[chromozome[i]][chromozome[j]] * startingPlacement[i].communication[j]);
		}
	}

	return tmp/2;	//every packet counted 2 times
}

int migrationDistance (std::vector <int> chromozome){
	int tmp = 0;

	for(int i = 0; i < numVMs; ++i){
		if(chromozome[i] != startingPlacement[i].nodePlacement){
			++tmp;
			VMRAMdistance += startingPlacement[i].RAM;
		}
	}

	return tmp;
}

int main (int argC, char ** argV){

	if(argC != 4){
		printUsage(argV[0]);
		return 0;
	}

	if(readDTC(argV[1]) != 0){
		return -1;
	}

	if(sscanf(argV[2], "%d", &clusterLineParam) != 1){
		return -2;
	}

	if(sscanf(argV[3], "%d", &clusterIncreaseParam) != 1){
		return -3;
	}

	std::vector<cluster> cls = createClusters();

	std::vector <int> placement, solution;
	placement.reserve(numVMs);
	solution.reserve(numVMs);
	for(auto & it : startingPlacement){
		placement.push_back(it.nodePlacement);
		solution.push_back(it.nodePlacement);
	}

	placeClusters(cls, solution);

	std::cout << "Solution found. Starting conf has communication load: " << getCommSum(placement) << std::endl;
	std::cout << "Proposed solution:" << std::endl;
	for(auto it : solution){
		std::cout << (it + 1) << " ";
	}

	VMRAMdistance = 0;
	std::cout << std::endl << std::endl << "Communication load: " << getCommSum(solution) << std::endl;
	std::cout << "# of migrations required: " << migrationDistance(solution) << std::endl;
	std::cout << "RAM to migrate: " << VMRAMdistance << std::endl;

	return 0;
}
