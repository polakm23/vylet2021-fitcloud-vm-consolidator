#include <iostream>
#include <vector>
#include <random>
#include <string>
#include <fstream>
#include <cstdio>
#include <limits>
#include <utility>
#include <algorithm>

std::default_random_engine RNG(std::random_device{}());

int numNodes = -1, nodeCores = -1, nodeRAM = -1, numVMs = -1;

std::vector <std::vector <int>> nodeDistances;

int VMRAMdistance;

class virtualMachine {
public:
	int cores, RAM, nodePlacement;
	std::vector <int> communication;

	virtualMachine(int c, int r, int n):cores(c), RAM(r), nodePlacement(n){
		communication.reserve(numVMs);
	}

	virtualMachine(const virtualMachine & vm){
		cores = vm.cores;
		RAM = vm.RAM;
		nodePlacement = vm.nodePlacement;
		communication = vm.communication;
	}
};

std::vector <virtualMachine> startingPlacement;

int populationSize, numOfGenerations, numOfElites, eliteRepetitions, crossoverProbability, mutationProbability;
int selectMax;

int readDTC(char * filePath){

	std::fstream file;
	file.open(filePath, std::fstream::in);
	if(!file.is_open()){
		std::cerr << "Failed to open datacentre file" << std::endl;
		return -1;
	}

	std::string tmp;

	file >> tmp >> numNodes;
	if(!file.good() || tmp != "#nodes:" || numNodes <= 0){
		std::cerr << "Error reading numNodes" << std::endl;
		file.close();
		return -2;
	}

	file >> tmp >> nodeCores;
	if(!file.good() || tmp != "#nodeCores:" || nodeCores <= 0){
		std::cerr << "Error reading nodeCores" << std::endl;
		file.close();
		return -3;
	}

	file >> tmp >> nodeRAM;
	if(!file.good() || tmp != "#nodeRAM:" || nodeRAM <= 0){
		std::cerr << "Error reading nodeRAM" << std::endl;
		file.close();
		return -4;
	}

	file >> tmp;
	if(!file.good() || tmp != "NodeDistances"){
		std::cerr << "Error reading NodeDistances" << std::endl;
		file.close();
		return -5;
	}

	{
		std::vector <int> tmpV;
		for(int i = 0; i < numNodes; ++i){
			tmpV.push_back(0);
		}
		for(int i = 0; i < numNodes; ++i){
			nodeDistances.push_back(tmpV);
		}
	}

	char tmpC1 = 'x', tmpC2 = 'x';
	int id = -1, dist = -1;

	for(int j = 0; j < numNodes; ++j){

		file >> tmp >> tmpC1 >> id;
		if(!file.good() || tmp != "NodeName:" || tmpC1 != 'n' || id <= 0 || id > numNodes){
			std::cerr << "Error reading NodeName" << std::endl;
			file.close();
			return -6;
		}
		file >> tmp;
		if(!file.good() || tmp != "Distances:"){
			std::cerr << "Error reading Distances" << std::endl;
			file.close();
			return -7;
		}
		for(int i = 0; i < numNodes; ++i){
			file >> tmpC1 >> id >> tmpC2 >> dist;
			if(!file.good() || tmpC1 != 'n' || tmpC2 != '-' || dist < 0 || id <= 0 || id > numNodes){
				std::cerr << "Error reading distance" << std::endl;
				file.close();
				return -8;
			}
			nodeDistances[j][id-1] = dist;
		}

	}

	file >> tmp >> numVMs;
	if(!file.good() || tmp != "#virtualMachines:" || numVMs <= 0){
		std::cerr << "Error reading number of virtual machines" << std::endl;
		file.close();
		return -9;
	}
	startingPlacement.reserve(numVMs);
	
	int nodeID = 0, c = -1, r = -1, peekI = -1;
	for(int i = 0; i < numVMs; ++i){
		file >> c >> tmpC1 >> r;
		if(!file.good() || tmpC1 != '-' || c <= 0 || r <= 0){
			std::cerr << "Error reading VM params" << std::endl;
			file.close();
			return -10;
		}

		startingPlacement.emplace_back(c, r, nodeID);
		
		file.get(tmpC1);
		if(!file.good() || tmpC1 != ' '){
			std::cerr << "Error reading VMs" << std::endl;
			file.close();
			return -11;
		}
		peekI = file.peek();
		if(peekI == (int)'\n'){
			++nodeID;
		}
	}

	file >> tmp;
	if(!file.good() || tmp != "CommunicationMatrix:"){
		std::cerr << "Error reading Communication matrix" << std::endl;
		file.close();
		return -12;
	}

	int comm;
	for(int j = 0; j < numVMs; ++j){
		for(int i = 0; i < numVMs; ++i){
			file >> comm;
			if(!file.good() || comm < 0){
				std::cerr << "Error reading communication value" << std::endl;
				file.close();
				return -13;
			}

			startingPlacement[j].communication.push_back(comm);
		}
	}

	file.close();
	return 0;
}

void printUsage(char * name){
	std::cout << "Usage: " << name << " <.DTC file path> populationSize numOfGenerations numOfElites eliteRepetitions crossoverProbability mutationProbability" << std::endl;
	std::cout << "Both probabilities are expected in percentage as integers between 0 and 100." << std::endl;
}

int readParams(char ** params){
	int ret = 0;

	ret += sscanf(params[2],"%d",&populationSize);
	ret += sscanf(params[3],"%d",&numOfGenerations);
	ret += sscanf(params[4],"%d",&numOfElites);
	ret += sscanf(params[5],"%d",&eliteRepetitions);
	ret += sscanf(params[6],"%d",&crossoverProbability);
	ret += sscanf(params[7],"%d",&mutationProbability);

	if(ret != 6)
		return -1;

	if(populationSize <= 0){
		return -2;
	}else if(numOfGenerations <= 0){
		return -3;
	}else if(numOfElites <= 0 || numOfElites > populationSize){
		return -4;
	}else if(eliteRepetitions <= 0 || eliteRepetitions > numOfGenerations){
		return -5;
	}else if(crossoverProbability < 0 || crossoverProbability > 100){
		return -6;
	}else if(mutationProbability < 0 || mutationProbability > 100){
		return -7;
	}

	selectMax = (populationSize * (populationSize + 1)) / 2;

	return 0;
}

void generateFirstPopulation(std::vector<std::vector<int>> & pop){
	std::vector<int> tmp;
	pop.reserve(populationSize);

	for(int i = 0; i < populationSize; ++i){
		pop.push_back(tmp);
		pop[i].reserve(numVMs);
		for(int j = 0; j < numVMs; ++j){
			pop[i].push_back(RNG()%numNodes);
		}
	}

	for(int j = 0; j < numOfElites; ++j){
		for(int i = 0; i < numVMs; ++i){
			pop[j][i] = startingPlacement[i].nodePlacement;
		}
	}
}

class node{
public:
	int cores, RAM;

	node(int c, int r):cores(c), RAM(r){}

	node(const node & n){
		cores = n.cores;
		RAM = n.RAM;
	}
};

int getNumOfNonoverloadedNodes(std::vector<int> & chromozome){
	std::vector<node> cluster;

	for(int i = 0; i < numNodes; ++i){
		cluster.emplace_back(0,0);
	}

	for(int i = 0; i < numVMs; ++i){
		cluster[chromozome[i]].cores += startingPlacement[i].cores;
		cluster[chromozome[i]].RAM += startingPlacement[i].RAM;
	}

	int tmp = numNodes;
	for(int i = 0; i < numNodes; ++i){
		if(cluster[i].cores > nodeCores || cluster[i].RAM > nodeRAM){
			--tmp;
		}
	}
	
	return tmp;
}

long long int getCommSum(std::vector<int> & chromozome){
	long long int tmp = 0;

	for(int i = 0; i < numVMs; ++i){
		for(int j = 0; j < numVMs; ++j){
			tmp += (long long int)(nodeDistances[chromozome[i]][chromozome[j]] * startingPlacement[i].communication[j]);
		}
	}

	return tmp/2;	//every packet counted 2 times
}

void calculateFitness(std::vector<long long int> & fit, std::vector<std::vector<int>> & pop){
	int numOfNonoverloadedNodes;
	for(int i = 0; i < populationSize; ++i){
		numOfNonoverloadedNodes = getNumOfNonoverloadedNodes(pop[i]);

		if(numOfNonoverloadedNodes == numNodes){
			fit[i] = getCommSum(pop[i]);
		}else{
			fit[i] = std::numeric_limits<long long int>::max() - (long long int)numOfNonoverloadedNodes;
		}
	}
}

std::vector <std::vector <int>> elitism (std::vector<long long int> & fit, std::vector<std::vector<int>> & pop){

	std::vector<long long int> f (fit);
	std::vector<std::vector<int>> p (pop);

	int i;
	while(p.size() > (size_t)numOfElites){
		i = std::max_element(f.begin(),f.end()) - f.begin();
		p.erase(p.begin() + i);
		f.erase(f.begin() + i);
	}

//	std::cout << "ELITE " << p.size() << std::endl;
	return p;
}

void mutation (std::vector<int> & chromozome){
/*	for(int i = 0; i < numVMs; ++i){
		if(((RNG()%100) + 1) < (unsigned int)mutationProbability){
			chromozome[i] = RNG()%numNodes;
		}
	}*/
	chromozome[RNG()%chromozome.size()] = RNG()%numNodes;
}

void crossover (std::vector <int> & parent1, std::vector <int> & parent2, std::vector <int> & child){
	int split = RNG()%numVMs;

	for(int i = 0; i < numVMs; ++i){
		if(i < split){
			child[i] = parent1[i];
		}else{
			child[i] = parent2[i];
		}
	}
}

int selection (std::vector<std::pair<int, long long int>> & sortedFit){

	int rand = (RNG() % selectMax) + 1, index = 1, i = 1;

	for(; i < rand; ++index){
		i += index + 1;
	}

	return sortedFit[index-1].first;
}

long long int evolutionBaby (std::vector<int> & solution){

	std::vector<std::vector <int>> population, elites;
	std::vector<long long int> fitness;
	for(int i = 0; i < populationSize; ++i){
		fitness.push_back(-1);
	}

	generateFirstPopulation(population);

	int generation = 0, elitesRepeat = 0;

	while(generation < numOfGenerations && elitesRepeat <= eliteRepetitions){

		calculateFitness(fitness, population);

		std::vector<std::vector <int>> newElites = elitism(fitness, population);
		std::sort(newElites.begin(), newElites.end());

		if(elites == newElites){
			++elitesRepeat;
		}else{
			elites = newElites;
			elitesRepeat = 0;
		}

		std::vector<std::vector <int>> tmpSelection (population);
		std::vector<std::pair<int, long long int>> sortedFitness;
		for(int i = 0; i < populationSize; ++i){
			sortedFitness.emplace_back(i,fitness[i]);
		}
		std::sort(sortedFitness.begin(), sortedFitness.end(), [](std::pair<int, long long int> a, std::pair<int, long long int> b){
			return a.second > b.second;
		});

		//std::cout << elites.size() << std::endl;
		for(int i = 0; i < populationSize; ++i){
			if(i < numOfElites && generation != 0){
				population[i] = elites[i];
			}else{
				if((RNG()%100 + 1) < (unsigned int)crossoverProbability){
					crossover(tmpSelection[selection(sortedFitness)], tmpSelection[selection(sortedFitness)], population[i]);
				}
				if(((RNG()%100) + 1) < (unsigned int)mutationProbability){
					mutation(population[i]);
				}
			}
		}

		//std::cout << "gen" << std::endl;
		++generation;
	}

	calculateFitness(fitness, population);

	int resIndex = (int)(std::min_element(fitness.begin(), fitness.end()) - fitness.begin());

	if(getNumOfNonoverloadedNodes(population[resIndex]) == numNodes){
		solution = population[resIndex];
		return fitness[resIndex];
	}
	return -1;
}

int migrationDistance (std::vector <int> chromozome){
	int tmp = 0;

	for(int i = 0; i < numVMs; ++i){
		if(chromozome[i] != startingPlacement[i].nodePlacement){
			++tmp;
			VMRAMdistance += startingPlacement[i].RAM;
		}
	}

	return tmp;
}

int main (int argC, char ** argV){

	if(argC != 8){
		printUsage(argV[0]);
		return 0;
	}

	if(readParams(argV) != 0){
		std::cerr << "There was an error during argument parsing." << std::endl;
		return -1;
	}

	if(readDTC(argV[1]) != 0){
		return -2;
	}

	std::vector <int> solution;

	long long int res = evolutionBaby(solution);

	if(res < 0){
		std::cout << "Unable to find satisfying solution..." << std::endl;
		return 0;
	}

	std::cout << "Found solution: " << std::endl;
	for(auto it : solution){
		std::cout << (it + 1) << " ";
	}

	VMRAMdistance = 0;
	std::cout << std::endl << std::endl << "Communication load: " << res << std::endl;
	std::cout << "# of migrations required: " << migrationDistance(solution) << std::endl;
	std::cout << "RAM to migrate: " << VMRAMdistance << std::endl;

	return 0;
}
