#!/bin/bash

echo "Running heu now..."

for type in {"random","typed"}
do
for diff in {"light","medium","hard"}
do

	for i in ./instance/$type/$diff/*
	do

		for line in {5,15,30}
		do
			./bin/heuristika.out $i $line 10 >> ./reseni/heuristika/$type/$diff/${i##*/}
		done

		echo " $i done"

	done

done
done

echo "Heu done. Running genetics now..."

for type in {"random","typed"}
do
for diff in {"light","medium","hard"}
do

	for prog in {"genetika.out","genetikaKrocena.out"}
	do

		for i in ./instance/$type/$diff/*
		do

			./bin/$prog $i 300 350 20 80 70 4 >> ./reseni/${prog%%\.*}/$type/$diff/${i##*/}
			echo "		$i done"
		done
	done
	echo "	$type done"
done
done

echo "Genetics done."
