#!/bin/bash

for i in {201..300}
do
	./generator/gen.out ./generator/topology.TPG 70 5 > ./instance/random/light/$i.DTC
	./generator/gen.out ./generator/topology.TPG 45 5 > ./instance/random/medium/$i.DTC
	./generator/gen.out ./generator/topology.TPG 35 5 > ./instance/random/hard/$i.DTC
	./generator/typeGen.out ./generator/topology.TPG 70 5 > ./instance/typed/light/$i.DTC
	./generator/typeGen.out ./generator/topology.TPG 45 5 > ./instance/typed/medium/$i.DTC
	./generator/typeGen.out ./generator/topology.TPG 35 5 > ./instance/typed/hard/$i.DTC

	echo "$i done"
done
