import matplotlib.pyplot as plt
import numpy as np

toProcess = 300
title = "genLow"

startsFile = open('startgenLow.txt', 'r')
heuResFile = open('heuristikagenLowRes.txt', 'r')
heuRAMFile = open('heuristikagenLowRAM.txt', 'r')
genResFile = open('genetikagenLowRes.txt', 'r')
genRAMFile = open('genetikagenLowRAM.txt', 'r')
krocResFile = open('genetikaKrocenagenLowRes.txt', 'r')
krocRAMFile = open('genetikaKrocenagenLowRAM.txt', 'r')

starts = []
heuBestRes = []
heuBestRam = []
genBestRes = []
genBestRam = []
krocBestRes = []
krocBestRam = []

for i in range(toProcess):
	starts.append(int(startsFile.readline()))
	
	iheuBestRes = 9999999999999999999999999999999999999999999
	iheuBestRAM = 0
	for i in range(4):
		tmpRes = heuResFile.readline()
		tmpRAM = heuRAMFile.readline()

		if tmpRes == '()\n':
			break

		if int(tmpRes) < iheuBestRes:
			iheuBestRes = int(tmpRes)
			iheuBestRAM = int(tmpRAM)

	heuBestRes.append(iheuBestRes);
	heuBestRam.append(iheuBestRAM);


	ikrocBestRes = 9999999999999999999999999999999999999999999
	ikrocBestRAM = 0
	tmpRes = krocResFile.readline()
	tmpRAM = krocRAMFile.readline()
	if tmpRes != '()\n':
		ikrocBestRes = int(tmpRes)
		ikrocBestRAM = int(tmpRAM)
		krocResFile.readline()
		krocRAMFile.readline()
	krocBestRes.append(ikrocBestRes)
	krocBestRam.append(ikrocBestRAM)


	igenBestRes = 9999999999999999999999999999999999999999999
	igenBestRAM = 0
	tmpRes = genResFile.readline()
	tmpRAM = genRAMFile.readline()
	if tmpRes != '()\n':
		igenBestRes = int(tmpRes)
		igenBestRAM = int(tmpRAM)
		genResFile.readline()
		genRAMFile.readline()
	genBestRes.append(igenBestRes)
	genBestRam.append(igenBestRAM)
	

#print(starts)
#print(heuBestRes)
#print(heuBestRam)
#print(krocBestRes)
#print(krocBestRam)
#print(genBestRes)
#print(genBestRam)

if(len(heuBestRes) != len(heuBestRam) or len(krocBestRes) != len(krocBestRam) or len(genBestRes) != len(genBestRam)):
	sys.exit("ResLen != RamLen")

if(len(heuBestRes) != toProcess or len(krocBestRes) != toProcess or len(genBestRes) != toProcess):
	sys.exit("different length of some best results array")

heuFailed = 0
genFailed = 0
krocFailed = 0
heuImproved = []
heuImprovedRAM = []
genImproved = []
genImprovedRAM = []
krocImproved = []
krocImprovedRAM = []

for i in range(0, toProcess):
	if heuBestRes[i] >= starts[i]:
		heuFailed += 1
	else:
		heuImproved.append(heuBestRes[i] / starts[i] * 100)
		heuImprovedRAM.append(heuBestRam[i])

	if genBestRes[i] >= starts[i]:
		genFailed += 1
	else:
		genImproved.append(genBestRes[i] / starts[i] * 100)
		genImprovedRAM.append(genBestRam[i])

	if krocBestRes[i] >= starts[i]:
		krocFailed += 1
	else:
		krocImproved.append(krocBestRes[i] / starts[i] * 100)
		krocImprovedRAM.append(krocBestRam[i])

print("Heuristika Failed: ", heuFailed, " Prumer improved: ", np.mean(heuImproved), " Prumer RAM: ", np.mean(heuImprovedRAM))
print("Genetika Failed: ", genFailed, " Prumer improved: ", np.mean(genImproved), " Prumer RAM: ", np.mean(genImprovedRAM))
print("Krocena genetika Failed: ", krocFailed, " Prumer improved: ", np.mean(krocImproved), " Prumer RAM: ", np.mean(krocImprovedRAM))

fig, ax = plt.subplots()

data = [heuImproved, genImproved, krocImproved]
leg = ["heuristic","GA - wild","GA - calm"]
ax.hist(data, histtype = 'bar', label = leg)
ax.legend()
ax.set_title(title)

plt.show()
