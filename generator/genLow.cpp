#include <iostream>
#include <random>
#include <vector>
#include <string>
#include <queue>
#include <utility>
#include <functional>
#include <fstream>
#include <cstdio>

//#define defaultStopProbability 15
//#define defaultClusterProbability 20

#define maxPackets 1000
#define maxNonClusterPackets 300

#define stateNotFound 0
#define stateOpened 1
#define stateClosed 2

#define charNode 'n'
#define charSwitch 's'

std::vector<int> numCoresPossibilities {/*8,12,16,20,24,28,32,48,56,64,96,112,*/128,256};
std::vector<int> RAMSizes {/*16,32,64,128,160,*/192,256,384,512,768,1024};

std::vector<int> virtualCoresPossibilities {1,2,4,8,16,24,32,64};
std::vector<int> virtualRamPossibilities {1,2,4,8,16,32,64,/*128 ,160,192,256*/};

std::default_random_engine RNG(std::random_device{}());

class node {
public:
	std::string name;
	std::vector<std::reference_wrapper<node>> neighbours;
	std::vector<std::pair<std::reference_wrapper<node>, int>> distances;
	short state;

	node(std::string & n):name(n),neighbours(),distances(),state(stateNotFound){}

	node(const node & n){
		name = n.name;
		neighbours = n.neighbours;
		distances = n.distances;
		state = n.state;
	}
};

class virtualMachine {
public:
	int numCores;
	int RAMSize;
	std::vector<int> packetsSend;
	bool lastInNode;

	//virtualMachine(int cores, int ram):numCores(cores),RAMSize(ram){};

	virtualMachine():lastInNode(false){
		numCores = virtualCoresPossibilities[RNG()%virtualCoresPossibilities.size()];
		RAMSize = virtualRamPossibilities[RNG()%virtualRamPossibilities.size()];

		if(numCores > RAMSize){
			std::swap(numCores, RAMSize);
		}
	};

	virtualMachine(const virtualMachine & vm){
		numCores = vm.numCores;
		RAMSize = vm.RAMSize;
		packetsSend = vm.packetsSend;
		lastInNode = vm.lastInNode;
	}
};

void generateNodes(int & nodeCores, int & nodeRAM)
{
	nodeCores = numCoresPossibilities[RNG() % numCoresPossibilities.size()];
	nodeRAM = RAMSizes[RNG() % RAMSizes.size()];
}

void generateVirtualMachines (std::vector<virtualMachine> & virtualMachines, int maxCores, int maxRAM, int numNodes, int filledNodes, int stopProbability){
	while(virtualCoresPossibilities.back() > maxCores){
		virtualCoresPossibilities.pop_back();
	}

	while(virtualRamPossibilities.back() > maxRAM){
		virtualRamPossibilities.pop_back();
	}

	int cnt = 0;
	int tmpRAM;
	int tmpCPU;
	while(cnt < numNodes){
		tmpRAM = 0;
		tmpCPU = 0;
		while(true){
			virtualMachine tmp;

			if( ((cnt < filledNodes) && (tmp.numCores + tmpCPU <= maxCores) && (tmp.RAMSize + tmpRAM <= maxRAM))
										||
			(((int)(RNG()%100 + 1) > stopProbability) && (tmp.numCores + tmpCPU <= maxCores) && (tmp.RAMSize + tmpRAM <= maxRAM)) ){
				virtualMachines.push_back(tmp);
				tmpCPU += tmp.numCores;
				tmpRAM += tmp.RAMSize;
			}else{
				virtualMachines.back().lastInNode = true;
				break;
			}
		}
		++cnt;
	}
}

void generateCommunication (std::vector<virtualMachine> & virtMachines, int clusterProbability){
	for(auto & it : virtMachines){
		it.packetsSend.reserve(virtMachines.size());

		for(size_t i = 0; i < virtMachines.size(); ++i){
			it.packetsSend.push_back(0);
		}
	}

	for(size_t i = 0; i < virtMachines.size(); ++i){
		for(size_t j = 0; j < virtMachines[i].packetsSend.size(); ++j){
			if(i == j){
				continue;
			}else if(virtMachines[i].packetsSend[j] != 0){
				continue;
			}else{
				if((int)(RNG()%100 + 1) <= clusterProbability){
					virtMachines[i].packetsSend[j] = RNG()%maxPackets;
				}else{
					virtMachines[i].packetsSend[j] = RNG()%maxNonClusterPackets;
				}
				virtMachines[j].packetsSend[i] = virtMachines[i].packetsSend[j];
			}
		}
	}
}

void printUsage(char ** argV){
	std::cerr << "Usage: " << argV[0] << " [topologyFilePath fillingStopProbability clusterProbability]" << std::endl;
	std::cerr << "Program expect probability values as numbers from Z >= 0 and <= 100." << std::endl;
}

int readTopology(char * filePath, int & numNodes, int & filledNodes, std::vector<node> & topology){
	std::fstream file;
	file.open(filePath,std::fstream::in);
	if(!file.is_open()){
		std::cerr << "Failed to open topology file" << std::endl;
		return -1;
	}

	int graphNodes;

	file >> graphNodes >> numNodes >> filledNodes;
	if(!file.good() || numNodes <= 0 || filledNodes <= 0 || filledNodes > numNodes || graphNodes < numNodes){
		std::cerr << "numNodes fail" << std::endl;
		file.close();
		return -2;
	}

	std::string tmp;

	for(int i = 0; i < graphNodes; ++i){
		file >> tmp;
		if(!file.good()){
			std::cerr << "nodeName fail" << std::endl;
			file.close();
			return -3;
		}else if(tmp[0] != charNode && tmp[0] != charSwitch){
			std::cerr << "node identity fail" << std::endl;
			file.close();
			return -4;
		}

		topology.push_back(tmp);
	}

//TODO duplicitni jmena a neexistujici vrcholy

	int tmpI;

	for(int i = 0; i < graphNodes; ++i){
		file >> tmp >> tmpI;	//skip name of actual node in tmp
		if(!file.good()){
			std::cerr << "node neighbours read fail" << std::endl;
			file.close();
			return -5;
		}


		for(int j = 0; j < tmpI; ++j){
			file >> tmp;
			if(!file.good()){
				std::cerr << "node neighbours read fail" << std::endl;
				file.close();
				return -6;
			}

			for(auto & it : topology){
				if(it.name == tmp){
					topology[i].neighbours.push_back(it);
				}
			}
		}
	}

	file.close();
	return 0;
}

void calcDistances(std::vector<node> & topology){
	for(auto & it : topology){
		std::queue<std::pair<std::reference_wrapper<node>,int>> q;
		for(auto & iit : topology){
			iit.state = stateNotFound;
		}

		q.push(std::make_pair<std::reference_wrapper<node>,int>(it,0));
		it.state = stateOpened;

		while(!q.empty()){
			for(auto & iit : q.front().first.get().neighbours){
				if(iit.get().state == stateNotFound){
					iit.get().state = stateOpened;
					q.push(std::make_pair<std::reference_wrapper<node>,int>(iit.get(), q.front().second+1));
				}
			}

			q.front().first.get().state = stateClosed;
			it.distances.push_back(std::pair<std::reference_wrapper<node>,int>(q.front().first.get(), q.front().second));
			q.pop();
		}

		if(topology.size() != it.distances.size()){
			std::cerr << "Found unreachable node or switch... Funy..." << std::endl;
			exit(-666);
		}
	}

}

int main (int argC, char ** argV){
	if(argC != 4){
		printUsage(argV);
		return -1;
	}

	int stopProbability = -1, clusterProbability = -1;
	int numNodes = -1, filledNodes = -2;

	int ret = 0;
	ret += sscanf(argV[2],"%d",&stopProbability);
	ret += sscanf(argV[3],"%d",&clusterProbability);

	if(ret != 2 || stopProbability < 0 || stopProbability > 100 || clusterProbability < 0 || clusterProbability > 100){
		printUsage(argV);
		return -2;
	}

	std::vector<node> topology;

	if(readTopology(argV[1], numNodes, filledNodes, topology) != 0){
		return -3;
	}

	int nodeRAM, nodeCores;
	generateNodes( nodeCores, nodeRAM);

	calcDistances(topology);

	std::vector<virtualMachine> virtualMachines;
	generateVirtualMachines(virtualMachines, nodeCores, nodeRAM, numNodes, filledNodes, stopProbability);

	generateCommunication(virtualMachines, clusterProbability);

	std::cout << "#nodes: " << numNodes << std::endl;
	std::cout << "#nodeCores: " << nodeCores << " #nodeRAM: " << nodeRAM << std::endl << std::endl;

	std::cout << "NodeDistances" << std::endl;
	for(auto & it : topology){
		if(it.name[0] == charSwitch){
			continue;
		}
		std::cout << "NodeName: " << it.name << " Distances: ";
		for(auto & iit : it.distances){
			if(iit.first.get().name[0] == charNode){
				std::cout << " " << iit.first.get().name << "-" << iit.second;
			}
		}

		std::cout << std::endl;
	}

	std::cout << std::endl << std::endl << "#virtualMachines: " << virtualMachines.size() << std::endl;

	for(auto & it : virtualMachines){
		std::cout << it.numCores << "-" << it.RAMSize << " ";
		if(it.lastInNode){
			std::cout << std::endl;
		}
	}
	std::cout << std::endl << std::endl << "CommunicationMatrix:" << std::endl;

	for(auto & it : virtualMachines){
		for(auto & iit : it.packetsSend){
			std::cout << iit << " ";
		}
		std::cout << std::endl;
	}
}
